var seconds = 0;
var isPlaying = true;
let el = document.getElementById('seconds-counter');
let pause = document.getElementById('pause');
let play = document.getElementById('play');
let save = document.getElementById('save');

function timerUpdate(el, secondsTotal) {
    let unformattedSecs = secondsTotal;

    let secs = unformattedSecs % 60;
    let mins = Math.floor(unformattedSecs / 60) % 60;
    let hrs = Math.floor(unformattedSecs / 3600) % 24;

    secs = ("0" + secs).slice(-2);
    mins = ("0" + mins).slice(-2);
    hrs = ("0" + hrs).slice(-2);

    el.innerText = hrs + ':' + mins + ':' + secs;
}

function incrementSeconds() {
    if (isPlaying === true) {
        seconds += 1;
    }
    timerUpdate(el, seconds);
}

function idle() {
    timerUpdate(el, seconds);
}


pause.onclick = function() {
    isPlaying = false;
}

play.onclick = function() {
    isPlaying = true;
}

save.onclick = function() {
    let parentBox = document.getElementById('wrapper');
    let clone = document.getElementById('seconds-counter');
    let sclone = clone.cloneNode(true);
    parentBox.appendChild(sclone);
}

setInterval(incrementSeconds, 1000);
